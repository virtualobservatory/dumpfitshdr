from setuptools import setup

setup(
  name='dumpfitshdr',
  version='0.5.0',
  author='Yan Grange',
  author_email='grange@astron.nl',
  scripts=['dump_headers.py'],
  download_url='https://git.astron.nl/virtualobservatory/dumpfitshdr',
  license='LICENSE.txt',
  description='Simple tool to dump fits headers to text files.',
  long_description=open('README.md').read(),
  install_requires=[
       "astropy>=4.1"
  ],
)
