#!/usr/binenv python
# SPDX-License-Identifier: Apache-2.0 

from astropy.io import fits
from os import mkdir
import glob

try:
    mkdir("fits_headers")
except FileExistsError:
    pass       # We don't care if the dir already exists

allfits = glob.glob("*.fits")
allfits.extend(glob.glob("*.fits.fz"))

for fitsfile in allfits:
   hdrs = list()
   with fits.open(fitsfile) as fhand:
      for hdu in fhand:
         hdrs.append(hdu.header)

   for ctr, hdr in enumerate(hdrs):
       hdr.totextfile(f"fits_headers/{fitsfile}.{ctr}.hdr", overwrite=True)
