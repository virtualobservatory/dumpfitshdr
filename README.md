# dumpfitshdr

Very simple python script to dump the headers of all fits in a directory (based on extensions fits and fits.fz) to files with extension .hdr. 

Execute this python script from inside a directory containing the fits files that need to be processed.
